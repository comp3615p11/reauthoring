
// style the sidebar navigation by implementing a collapsible menu
$(function(){
  $('#navigation').find('.toctree-l1').each(function () {
    var element = $(this);
    element.find('.toctree-l2').hide();
    var caret = $('<span class="caret-right"></span>');
    caret.click(function(e) {
      element.find('.toctree-l2').toggle();
      caret.toggleClass("caret-down");
      if (element.children().length > 1) {
        element.prepend(caret);
      }
    });
    if (element.children().length > 1) {
      element.prepend(caret);
    }
  });
});
